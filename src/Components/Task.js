import React from "react";

const Task = props => {
    return (
            <ul className='list-group px-5 mt-5'>
                <li className='list-group-item d-flex justify-content-between'>{props.task}
                    <button className='btn btn-primary' onClick={props.delete}>Delete</button>
                </li>
            </ul>
    );
};

export default Task;