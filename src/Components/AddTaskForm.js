import React from "react";


const AddTaskForm = props => {
     return (
         <form onSubmit={props.change}>
             <div className="input-group mb-3 mt-5 px-5">
                 <input type="text"
                        placeholder="введите задачу"
                        className="form-control"
                        onChange={props.inputChange}
                        value={props.value}
                 />
                 <button type="submit" className='btn btn-outline-secondary'>Click me</button>
             </div>
         </form>
     )
}

export default AddTaskForm;