import './App.css';
import AddTaskForm from "./Components/AddTaskForm";
import Task from "./Components/Task";
import {useState} from "react";
import {nanoid} from "nanoid";

const App = () => {
    const [task, setTask] = useState([
        {task:'Buy milk', id: 1},
        {task:'Walk with dog', id: 2},
        {task:'Do homework', id: 3}
    ]);

    const [input, setInput] = useState('');


    const newTask = () => {
        if (input) {
            const newObj = {task: input, id: nanoid(10)};
            setTask((task) => {
                return [...task, newObj];
            });
            setInput('');
        }
    };

    const remove = id => {
        const myIndex = task.findIndex(task => task.id === id);
        const copy = [...task];
        copy.splice(myIndex, 1);
        setTask(copy);
    };

    const taskComponents = task.map(myTask => (
        <Task key={myTask.id}
              task={myTask.task}
              delete={() => remove(myTask.id)}
        />
    ));



  return (
    <div className="App">
        <AddTaskForm
            inputChange={event => setInput(event.target.value)}
            change={(e) => newTask(e.preventDefault())}
            value={input}
        />
        {taskComponents}
    </div>
  );
}

export default App;
